
from stacker.blueprints.base import Blueprint, CFNParameter
from troposphere import Ref, Output, Sub, GetAtt, Join
from troposphere.awslambda import Permission
from troposphere.iot import LambdaAction, Action, TopicRulePayload, TopicRule

import util

class IotRules(Blueprint):
    VARIABLES = {
        'Rules': {
            'type': list,
            'description': 'List of rules to be deployed'
        },
        'Lambdas': {
            'type': dict,
            'description': 'Lambda names used in the rules'
        }
    }

    def resolve_variables(self, provided_variables):
        # Override default behavior to dynamically create CFN params for each table in the Tables dict
        super(IotRules, self).resolve_variables(provided_variables)

        util.enhance_variables(self, provided_variables, 'Lambdas')


    def create_rules(self, variables):
        for rule in variables['Rules']:

            rule_name = rule['name']
            lambda_logical_name = rule['target']
            lambda_physical_name = variables['Lambdas'][lambda_logical_name]

            rule = self.template.add_resource(
                TopicRule(
                    rule_name,
                    TopicRulePayload=TopicRulePayload(
                        AwsIotSqlVersion='2016-03-23',
                        RuleDisabled=False,
                        Sql=rule['sql'],
                        Actions=[
                            Action(
                                Lambda=LambdaAction(FunctionArn=Sub('arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:function:' + lambda_physical_name))
                            )
                        ]
                    )
                )
            )

            self.template.add_resource(
                Permission(
                    lambda_logical_name + 'Permission' + rule_name,
                    Action='lambda:InvokeFunction',
                    FunctionName=lambda_physical_name,
                    Principal='iot.amazonaws.com',
                    SourceAccount=Ref('AWS::AccountId'),
                    SourceArn=Sub('arn:aws:iot:${AWS::Region}:${AWS::AccountId}:rule/${rule}', rule=Ref(rule))
                )
            )

            self.template.add_output(
                Output(
                    rule.title,
                    Description='The IoT rule name',
                    Value=Ref(rule)
                )
            )


    def create_template(self):
        self.template.add_version('2010-09-09')
        self.template.add_description('Create lambda resources')

        variables = self.get_variables()
        self.create_rules(variables)
