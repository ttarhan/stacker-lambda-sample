import awacs.logs
import awacs.sts
import awacs.dynamodb
from awacs.aws import Allow, Statement, Principal
from awacs.aws import Policy as AcsPolicy
from stacker.blueprints.base import Blueprint, CFNParameter
from stacker.blueprints.variables.types import CFNString, CFNNumber
from stacker.variables import Variable
from troposphere import Ref, Output, Sub, GetAtt, Join
from troposphere.awslambda import Function, Code, Environment
from troposphere.iam import Role, Policy, PolicyType

import util

class Lambda(Blueprint):
    VARIABLES = {
        'Lambdas': {
            'type': list,
            'description': 'List of lambdas to be deployed'
        },
        'Tables': {
            'type': dict,
            'description': 'Table names used by the lambda functions'
        },
        'Environments': {
            'type': dict,
            'description': 'Default environment to pass to the lambda functions'
        }
    }

    def resolve_variables(self, provided_variables):
        # Override default behavior to dynamically create CFN params for each table in the Tables dict
        super(Lambda, self).resolve_variables(provided_variables)

        util.enhance_variables(self, provided_variables, 'Tables')

    def create_lambda_roles(self, variables):
        lambda_role = self.template.add_resource(
            Role(
                'LambdaRole',
                AssumeRolePolicyDocument=AcsPolicy(
                    Version='2012-10-17',
                    Statement=[
                        Statement(
                            Effect=Allow,
                            Action=[awacs.sts.AssumeRole],
                            Principal=Principal('Service', ['lambda.amazonaws.com'])
                        )
                    ]
                ),
                Policies=[
                    Policy(
                        PolicyName='lambda-log-access',
                        PolicyDocument=AcsPolicy(
                            Version="2012-10-17",
                            Statement=[
                                Statement(
                                    Action=[
                                        awacs.logs.CreateLogGroup,
                                        awacs.logs.CreateLogStream,
                                        awacs.logs.PutLogEvents
                                    ],
                                    Effect=Allow,
                                    Resource=['*']
                                )
                            ]
                        )
                    ),

                    Policy(
                        PolicyName='lambda-dynamo-acccess',
                        PolicyDocument=AcsPolicy(
                            Version="2012-10-17",
                            Statement=[
                                Statement(
                                    Action=[
                                        awacs.dynamodb.GetItem,
                                        awacs.dynamodb.DeleteItem,
                                        awacs.dynamodb.PutItem,
                                        awacs.dynamodb.UpdateItem,
                                        awacs.dynamodb.Query,
                                        awacs.dynamodb.Scan
                                    ],
                                    Effect=Allow,
                                    Resource=[Sub('arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${' + tableName + '}') for tableName in sorted(variables["Tables"].keys())]
                                )
                            ]
                        )
                    )
                ]
            )
        )

        self.template.add_output(
            Output(
                lambda_role.title + 'Arn',
                Description='Lambda Role ARN',
                Value=GetAtt(lambda_role, 'Arn')
            )
        )

        return lambda_role

    def create_lambda(self, lambda_role, variables):
        for lambda_params in variables["Lambdas"]:
            code = self.context.hook_data['lambda'][lambda_params.get('code', 'Default')]

            lambda_env = {}
            for key, value in sorted(variables.get('Environments', {}).iteritems()):
                lambda_env[key] = Sub('${' + value + '}')
            for key, value in sorted(lambda_params.get('environments', {}).iteritems()):
                lambda_env[key] = Sub('${' + value + '}')

            memory = 128
            if 'resource' in lambda_params:
                resource = lambda_params['resource']
                if 'memory' in resource:
                    memory = resource['memory']

            lambda_function = self.template.add_resource(
                Function(
                    lambda_params['name'] + 'Function',
                    FunctionName=self.context.namespace + self.context.namespace_delimiter + lambda_params['name'],
                    Description=lambda_params.get('description', lambda_params['name']),
                    Handler=lambda_params['handler'],
                    Role=GetAtt(lambda_role, "Arn"),
                    Code=code,
                    Runtime='nodejs6.10',
                    DependsOn="LambdaRole",
                    Environment=Environment(
                        Variables=lambda_env
                    ),
                    MemorySize=memory
                )
            )

            self.template.add_output(
                Output(
                    lambda_function.title,
                    Description='The lambda function name',
                    Value=Ref(lambda_function)
                )
            )

            self.template.add_output(
                Output(
                    lambda_function.title + "Arn",
                    Description='The lambda function ARN',
                    Value=GetAtt(lambda_function, 'Arn')
                )
            )

    def create_template(self):
        self.template.add_version('2010-09-09')
        self.template.add_description('Create lambda resources')

        variables = self.get_variables()
        lambda_role = self.create_lambda_roles(variables)
        self.create_lambda(lambda_role, variables)
