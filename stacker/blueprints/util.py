from stacker.variables import Variable
from stacker.blueprints.base import Blueprint, CFNParameter
from stacker.blueprints.variables.types import CFNString, CFNNumber

def enhance_variables(blueprint, provided_variables, input_name):
    variables = blueprint.get_variables()

    for name, value in sorted(variables.get(input_name, {}).iteritems()):
        blueprint.resolved_variables[name] = CFNParameter(name=name, value=value)
        provided_variables.append(Variable(name, value))

        blueprint.VARIABLES[name] = {
            'type': CFNString
        }
